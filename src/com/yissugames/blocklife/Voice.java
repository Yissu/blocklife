/**
 * Copyright (c) 2012, http://www.yissugames.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yissugames.blocklife;

import org.newdawn.slick.openal.Audio;
import org.newdawn.slick.openal.SoundStore;

public class Voice {

	public static final int welcomeANDplaytutorial = 1;
	
	public static void speak(int fileId)
	{
		String fileName = ((Integer) fileId).toString();
		if(fileName.length() == 1)
			fileName = "000" + fileName;
		else if(fileName.length() == 2)
			fileName = "00" + fileName;
		else if(fileName.length() == 3)
			fileName = "0" + fileName;
		
		Audio audio = ContentLoader.getOGGAudio(fileName, true);
		audio.playAsSoundEffect(1.0f, 1.0f, false);
		
		SoundStore.get().poll(0);
	}
}

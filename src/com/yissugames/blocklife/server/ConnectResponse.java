package com.yissugames.blocklife.server;

public class ConnectResponse {

	public String sessionId;
	
	public ConnectResponse() {
		
	}
	
	public ConnectResponse(String sessionId) {
		this.sessionId = sessionId;
	}
	
}

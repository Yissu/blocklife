package com.yissugames.blocklife.server;

public class ConnectRequest {

	public String username;
	
	public ConnectRequest() {
		
	}
	
	public ConnectRequest(String username) {
		this.username = username;
	}
	
}

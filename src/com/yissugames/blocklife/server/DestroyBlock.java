package com.yissugames.blocklife.server;

public class DestroyBlock {

	public int chunk;
	public int x;
	public int y;
	
	public DestroyBlock(int chunk, int x, int y) {
		this.chunk = chunk;
		this.x = x;
		this.y = y;
	}
	
	public DestroyBlock() {
		
	}
	
}

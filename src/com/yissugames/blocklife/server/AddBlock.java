package com.yissugames.blocklife.server;

public class AddBlock {

	public int chunk;
	public int x;
	public int y;
	public int block;
	
	public AddBlock(int chunk, int x, int y, int block) {
		this.chunk = chunk;
		this.x = x;
		this.y = y;
		this.block = block;
	}
	
	public AddBlock() {
		
	}
	
}

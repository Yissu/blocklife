package com.yissugames.blocklife.server;

import java.io.IOException;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Listener;
import com.yissugames.blocklife.gamelogic.Block;
import com.yissugames.blocklife.gamelogic.Chunk;
import com.yissugames.blocklife.gamelogic.DirtBlock;
import com.yissugames.blocklife.gamelogic.GrassBlock;
import com.yissugames.blocklife.gamelogic.LeavesBlock;
import com.yissugames.blocklife.gamelogic.PlankBlock;
import com.yissugames.blocklife.gamelogic.SerializableWorld;
import com.yissugames.blocklife.gamelogic.StoneBlock;
import com.yissugames.blocklife.gamelogic.WoodBlock;

public class ShooterClient {

	private static Client client;
	private static String sessionId;
	private static String username;
	public static void setUsername(String username) {
		ShooterClient.username = username;
	}
	public static String getUsername() {
		return ShooterClient.username;
	}
	
	public static String getSessionId() {
		return sessionId;
	}

	public static void setIdentify(String sessionId)
	{
		ShooterClient.sessionId = sessionId;
	}
	
	public static void init()
	{
		Kryo kryo = client.getKryo();
		kryo.register(int.class);
		
		kryo.register(ConnectRequest.class);
		kryo.register(ConnectResponse.class);
		kryo.register(Chunk.class);
		kryo.register(Block.class);
		kryo.register(Block[].class);
		kryo.register(Block[][].class);
		kryo.register(DirtBlock.class);
		kryo.register(GrassBlock.class);
		kryo.register(LeavesBlock.class);
		kryo.register(PlankBlock.class);
		kryo.register(StoneBlock.class);
		kryo.register(WoodBlock.class);
		
		kryo.register(BeginWorld.class);
		kryo.register(EndWorld.class);
		
		kryo.register(DestroyBlock.class);
		kryo.register(AddBlock.class);
		
		kryo.register(ChatPackage.class);
	}
	
	public static void connect(String ip) throws IOException
	{
		client = new Client(6666666, 6666666);
		init();
		client.start();
		client.connect(5000, ip, 6999, 6998);
	}
	
	public static void send(Object object)
	{
		client.sendUDP(object);
	}
	
	public static void sendDestroy(int chunk, int x, int y) {
		client.sendTCP(new DestroyBlock(chunk, x, y));
	}
	
	public static void sendAdd(int chunk, int x, int y, int block) {
		client.sendTCP(new AddBlock(chunk, x, y, block));
	}
	
	public static void sendMessage(String message) {
		client.sendUDP(new ChatPackage(username, message));
	}
	
	public static void addListener(Listener listener)
	{
		client.addListener(listener);
	}
	
	public static void close()
	{
		client.close();
	}
	
}

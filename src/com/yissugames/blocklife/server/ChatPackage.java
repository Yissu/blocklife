package com.yissugames.blocklife.server;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ChatPackage {

	public long initTime;
	public String date;
	public String sender;
	public String message;
	
	public ChatPackage(String sender, String message) {
		this.sender = sender;
		this.message = message;
	}
	
	public ChatPackage(ChatPackage pack) {
		this.sender = pack.sender;
		this.message = pack.message;
	}
	
	public ChatPackage() {
		
	}
	
	public void init() {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		this.date = sdf.format(new Date());
		this.initTime = System.currentTimeMillis();
	}
	
}

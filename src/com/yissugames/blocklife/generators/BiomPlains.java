/**
 * Copyright (c) 2012, http://www.yissugames.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yissugames.blocklife.generators;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import com.yissugames.blocklife.gamelogic.Block;
import com.yissugames.blocklife.gamelogic.Chunk;
import com.yissugames.blocklife.gamelogic.GrassBlock;

public class BiomPlains extends BiomGenerator {

	private int rand;
	private boolean spawnedTree = false;

	public BiomPlains() {
		super("Plains");
	}

	@Override
	public Block[][] generate(int last, int offset, Random random) {

		Block[][] blocks = new Block[32][256];

		int lastChange = 0;

		for (int i = 0; i < 32; i++) {
			int y = last;
			if (lastChange == 5) {
				y = randomRange(random, last - 2, last + 2);
				while (y == last)
					y = randomRange(random, last - 2, last + 2);
				lastChange = -1;
			}

			if (y < 0)
				y = 0;
			if (y >= 256)
				y = 255;
			last = y;
			blocks[i][y] = Block.Blocks[1].createBlockByThis().setPosition(
					i + offset, y);
			int i2;
			for (i2 = y + 1; i2 < y + 10; i2++)
				blocks[i][i2] = Block.Blocks[0].createBlockByThis()
						.setPosition(i + offset, i2);
			for (; i2 < 256; i2++)
				blocks[i][i2] = Block.Blocks[2].createBlockByThis()
						.setPosition(i + offset, i2);
			
			lastChange++;
		}

		return blocks;
	}

	public void treeSpawn(Random random, int i, int y, int offset,
			Block blocks[][]) {
		rand = random.nextInt(8);
		if (rand == 1) 
		{
			if (i < 31 && i > 0 && y >= 6) {
				if(blocks[i][y] instanceof GrassBlock)
					blocks[i][y] = Block.Blocks[0].createBlockByThis().setPosition(i + offset, y);
					
				for (int isd = 0; isd < 5; isd++) {
					blocks[i][y - isd - 1] = Block.Blocks[3].createBlockByThis()
							.setPosition(i + offset, y - isd - 1);
				}
				blocks[i - 1][y - 6] = Block.Blocks[4].createBlockByThis()
						.setPosition(i + offset - 1, y - 6);
				blocks[i + 1][y - 6] = Block.Blocks[4].createBlockByThis()
						.setPosition(i + offset + 1, y - 6);
				
				blocks[i][y - 6] = Block.Blocks[4].createBlockByThis().setPosition(
						i + offset, y - 6);
			}
			spawnedTree = true;
		}
	}

	public static int randomRange(Random random, int low, int high) {
		return random.nextInt(high - low) + low;
	}

}

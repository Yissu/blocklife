/**
 * Copyright (c) 2012, http://www.yissugames.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yissugames.blocklife.generators;

import java.util.Random;

import com.yissugames.blocklife.gamelogic.Block;

public abstract class BiomGenerator {

	private String biomName;
	private int seed;
	
	public BiomGenerator(String biomName)
	{
		this.biomName = biomName;
		/*this.seed = Integer.parseInt(seed.toLowerCase().replace('a', '1').replace('b', '2').replace('c', '3').replace('d', '4').replace('e', '5')
				.replace('f', '6').replace('g', '7').replace('h', '8').replace('i', '9').replace("j", "10").replace("k", "11")
				.replace("l", "12").replace("m", "13").replace("n", "14").replace("o", "15").replace("p", "16").replace("q", "17")
				.replace("r", "18").replace("s", "19").replace("t", "20").replace("u", "21").replace("v", "22").replace("w", "23")
				.replace("x", "24").replace("y", "25").replace("z", "26"));*/
	}
	
	public abstract Block[][] generate(int last, int offset, Random random);

	public int getSeed() {
		return seed;
	}
	
}

/**
 * Copyright (c) 2012, http://www.yissugames.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yissugames.blocklife;

import java.util.logging.Level;

import org.lwjgl.Sys;
import org.lwjgl.opengl.Display;

/**
 * It handels all screens and provide it public
 * 
 * @author Jonas
 * @version 1.0
 */
public class ScreenSystem {
	
	private static Screen savedScreen;
	private static Screen screen;
	private static Screen toSwitch;
	
	/**
	 * Setup the screen system at the moment not in use
	 * @deprecated
	 */
	public static void setup()
	{
		
	}
	
	public static void destory()
	{
		if(screen != null)
		{
			if(!screen.quit())
			{
				Display.destroy();
				Sys.alert("Critical system error", "Sorry but we must close BlockLife.");
				BlockLife.log.log(Level.SEVERE, screen.getClass().getName() + " has returned false on quit");
				System.exit(1);
			}
		}
	}
	
	public static void render()
	{
		if(screen != null)
			screen.render();
	}
	
	public static void set(Screen newscreen)
	{
		destory();
		screen = newscreen;
		if(!screen.init())
		{
			Display.destroy();			
			Sys.alert("Critical system error", "Sorry but we must close BlockLife.");
			BlockLife.log.log(Level.SEVERE, screen.getClass().getName() + " has returned false on init");
			System.exit(1);
		}
	}
	
	public static void setAndSave(Screen newscreen) 
	{
		savedScreen = screen;
		screen = newscreen;
		if(!screen.init())
		{
			Display.destroy();			
			Sys.alert("Critical system error", "Sorry but we must close BlockLife.");
			BlockLife.log.log(Level.SEVERE, screen.getClass().getName() + " has returned false on init");
			System.exit(1);
		}
	}
	
	public static void setToSavedScreen()
	{
		destory();
		screen = savedScreen;
	}
	
}

/**
 * Copyright (c) 2012, http://www.yissugames.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yissugames.blocklife.gamelogic;

import java.io.Serializable;

import com.yissugames.blocklife.ContentLoader;
import com.yissugames.blocklife.RenderSystem;

public abstract class Block implements Cloneable, Serializable {

	public static Block[] Blocks = new Block[255];
	
	public static void LoadBlocks()
	{
		Blocks[0] = new DirtBlock("Dirt", 0);
		Blocks[1] = new GrassBlock("Grass", 1);
		Blocks[2] = new StoneBlock("Stone", 2);
		Blocks[3] = new WoodBlock("Wood", 3);
		Blocks[4] = new LeavesBlock("Leaves", 4);
		Blocks[5] = new PlankBlock("Plank", 5);
	}
	
	private String name;
	private int id;
	protected int positionX;
	protected int positionY;
	private float resistance;
	
	public Block(String name, int id)
	{
		this.name = name;
		this.id = id;
	}
	
	public Block() {
		
	}
	
	public int getDropId()
	{
		return this.id;
	}
	
	public boolean isDestoryable()
	{
		return true;
	}
	
	public Block createBlockByThis()
	{
		try {
			return (Block) this.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Block setPositionX(int positionX)
	{
		this.positionX = positionX;
		return this;
	}
	
	public Block setPositionY(int positionY)
	{
		this.positionY = positionY;
		return this;
	}
	
	public Block setPosition(int positionX, int positionY)
	{
		this.positionX = positionX;
		this.positionY = positionY;
		return this;
	}
	
	public Block setResistance(float resistance)
	{
		this.resistance = resistance;
		return this;
	}
	
	public abstract void update(Chunk c);
	
	public void render(Chunk c)
	{
		update(c);
		RenderSystem.renderTileTexture(ContentLoader.getTexture("texture"), positionX * 32, positionY * 32, 32, 32, id);
	}
	
	public int getPositionX() { return this.positionX; }
	public int getPositionY() { return this.positionY; }
	
	public boolean equals(Object other)
	{
		if(other == null)
			return false;
		if(!(other instanceof Block))
			return false;
		
		Block otherBlock = (Block) other;
		if(otherBlock.getPositionX() == getPositionX() && otherBlock.getPositionY() == getPositionY())
			return true;
		
		return false;
			
	}
	
}

package com.yissugames.blocklife.gamelogic;

import java.util.ArrayList;

import com.yissugames.blocklife.gamelogic.Inventory.StackInfo;

public class CraftingRecipes {

	private static ArrayList<CraftingRecipe> recipes = new ArrayList<CraftingRecipe>();
	
	public static void init() {
		addRecipe(new StackInfo[][] { { new StackInfo(3, 1), null, null }, { null, null, null }, {null, null, null} }, "recipes.planksrecipe", new StackInfo(5, 4));
	}
	
	public static void addRecipe(CraftingRecipe recipe) {
		recipes.add(recipe);
	}
	
	public static void addRecipe(StackInfo[][] stacks, String languageId, StackInfo result) {
		recipes.add(new CraftingRecipe(stacks, languageId + ".id", languageId + ".description", result));
	}
	
	public static CraftingRecipe[] getRecipes() {
		return recipes.toArray(new CraftingRecipe[1]);
	}
	
	public static class CraftingRecipe {
		
		private StackInfo[][] stacks;
		private String name;
		private String description;
		private StackInfo result;
		
		public StackInfo[][] getStacks() {
			return stacks;
		}

		public String getName() {
			return name;
		}

		public String getDescription() {
			return description;
		}

		public StackInfo getResult() {
			return result;
		}
		
		public CraftingRecipe(StackInfo[][] stacks, String name, String description, StackInfo result)
		{
			this.stacks = stacks;
			this.name = name;
			this.description = description;
			this.result = result;
		}
		
	}
	
}

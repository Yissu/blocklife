/**
 * Copyright (c) 2012, http://www.yissugames.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yissugames.blocklife.gamelogic;

import java.io.Serializable;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import com.yissugames.blocklife.BlockLife;

public class BlockCamera implements Serializable {

	private float positionX;
	
	private float positionY;
	
	public BlockCamera(float positionX, float positionY)
	{
		this.positionX = positionX;
		this.positionY = positionY;
	}
	
	public BlockCamera()
	{
		this.positionX = 0;
		this.positionY = 0;
	}
	
	public void reset()
	{
		GL11.glLoadIdentity();
	}
	
	public void update(boolean ignoreInput)
	{
		if(!ignoreInput) {
			boolean left = Keyboard.isKeyDown(Keyboard.KEY_LEFT) || Keyboard.isKeyDown(Keyboard.KEY_A);
			boolean right = Keyboard.isKeyDown(Keyboard.KEY_RIGHT) || Keyboard.isKeyDown(Keyboard.KEY_D);
			boolean up = Keyboard.isKeyDown(Keyboard.KEY_UP) || Keyboard.isKeyDown(Keyboard.KEY_W);
			boolean down = Keyboard.isKeyDown(Keyboard.KEY_DOWN) || Keyboard.isKeyDown(Keyboard.KEY_S);
			
			if(left && this.positionX >= 0)
				this.positionX -= 0.8f;
			if(right)
				this.positionX += 0.8f;
			if(up)
				this.positionY -= 0.8f;
			if(down)
				this.positionY += 0.8f;
		}
		
		GL11.glTranslatef(-positionX, -positionY, 0);
	}
	
	public void setPosition(float positionX, float positionY)
	{
		this.positionX = positionX;
		this.positionY = positionY;
		
		GL11.glLoadIdentity();
		GL11.glTranslatef(-this.positionX, -this.positionY, 0);
	}
	
	public float getPositionX()
	{
		return this.positionX;
	}
	
	public float getPositionY()
	{
		return this.positionY;
	}
	
	public float getMouseX()
	{
		return Mouse.getX() + positionX;
	}
	
	public float getMouseY()
	{
		return BlockLife.DisplayHeight - (Mouse.getY() - positionY) - 1;
	}
	
	public float getScreenLeft()
	{
		return this.positionX;
	}
	
	public float getScreenRight()
	{
		return this.positionX + BlockLife.DisplayWidth;
	}
	
}

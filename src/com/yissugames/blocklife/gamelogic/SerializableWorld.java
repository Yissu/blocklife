package com.yissugames.blocklife.gamelogic;

import java.io.Serializable;
import java.util.ArrayList;

import com.yissugames.blocklife.gamelogic.Inventory.StackInfo;

public class SerializableWorld {

	public Chunk[] chunks;
	
	public SerializableWorld(World world) {
		ArrayList<Chunk> tmpChunks = world.getAllChunks();
		this.chunks = tmpChunks.toArray(new Chunk[tmpChunks.size()]);
	}
	
	public SerializableWorld(ArrayList<Chunk> chunks) {
		this.chunks = chunks.toArray(new Chunk[chunks.size()]);
	}
	
}

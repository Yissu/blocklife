/**
 * Copyright (c) 2012, http://www.yissugames.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yissugames.blocklife.gamelogic;

import java.io.Serializable;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.opengl.Texture;

import com.yissugames.blocklife.ContentLoader;
import com.yissugames.blocklife.RenderSystem;
import com.yissugames.blocklife.gamelogic.CraftingRecipes.CraftingRecipe;
import com.yissugames.blocklife.gamelogic.Inventory.StackInfo;

public class CraftingWindow implements Serializable {
	
	private Texture craftingField;
	private Texture hover;
	private boolean lastLeftState;
	private StackInfo[][] crafting;
	private TrueTypeFont font;
	private StackInfo result;
	private CraftingRecipe recipe;
	
	public CraftingWindow() {
		crafting = new StackInfo[3][3];
		craftingField = ContentLoader.getTexture("crafting");
		hover = ContentLoader.getTexture("quickbar.grid.select");
		font = ContentLoader.getFont("Arial", 0, 12);
	}
	
	public void render() {
		RenderSystem.renderTexture(craftingField, 10 * 34 + 25, 5);
		
		RenderSystem.renderRTexture(craftingField, 13 * 34 + 40, 39, 0, 0, 36, 36);
		
		int mouseX = Mouse.getX();
		int mouseY = Display.getHeight() - Mouse.getY() - 1;
		
		for(int x = 0; x < 3; x++) {
			for(int y = 0; y < 3; y++) {
				int startX = 10 * 34 + 25 + (x * 34);
				int startY = 5 + (y * 34);
				if(mouseX > startX && mouseX < startX + 34 &&
						mouseY > startY && mouseY < startY + 34)
					RenderSystem.renderTexture(hover, startX, startY);
				
				if(crafting[x][y] != null) {
					RenderSystem.renderTileTexture(ContentLoader.getTexture("texture"), startX + 4, startY + 4, 28, 28, crafting[x][y].getId());
					font.drawString(startX + 4, startY + 1, Integer.toString(crafting[x][y].getCount()), Color.white);
				}
			}
		}
		
		int startX = 13 * 34 + 40;
		int startY = 39;
		if(mouseX > startX && mouseX < startX + 36 &&
				mouseY > startY && mouseY < startY + 36)
			RenderSystem.renderTexture(hover, startX, startY);
		
		// Check if recipe is exist
		for(CraftingRecipe recipe: CraftingRecipes.getRecipes())
		{
			boolean status = true;
			StackInfo[][] stacks = recipe.getStacks();
			for(int x = 0; x < 3; x++) {
				for(int y = 0; y < 3; y++) {
					if(stacks[x][y] == null || crafting[x][y] == null)
					{
						if(stacks[x][y] == null && crafting[x][y] == null)
							;
						else
							status = false;
					} else
					if(stacks[x][y].getId() == crafting[x][y].getId() &&
							stacks[x][y].getCount() <= crafting[x][y].getCount())
						;
					else {
						status = false;
						break;
					}
				}
			}
			
			if(status) {
				RenderSystem.renderTileTexture(ContentLoader.getTexture("texture"), 13 * 34 + 44, 43, 28, 28, recipe.getResult().getId());
				result = recipe.getResult();
				this.recipe = recipe;
			} else {
				result = null;
				this.recipe = null;
			}
		}
	}
	
	public StackInfo update(StackInfo onMouse) {
		
		int mouseX = Mouse.getX();
		int mouseY = Display.getHeight() - Mouse.getY() - 1;
		
		for(int x = 0; x < 3; x++) {
			for(int y = 0; y < 3; y++) {
			int startX = 10 * 34 + 25 + (x * 34);
				int startY = 5 + (y * 34);
				if(mouseX > startX && mouseX < startX + 34 &&
						mouseY > startY && mouseY < startY + 34 && 
						Mouse.isButtonDown(0) && !lastLeftState)
				{
					if(onMouse != null) {
						if(crafting[x][y] == null) {
							crafting[x][y] = onMouse;
							onMouse = null;
						} else if(onMouse.getId() == crafting[x][y].getId()) {
							onMouse.setCount(crafting[x][y].add(onMouse.getCount()));
							if(onMouse.getCount() == 0)
								onMouse = null;
						}
					} else {
						if(crafting[x][y] != null) {
							onMouse = crafting[x][y];
							crafting[x][y] = null;
						}
					}
				}
			}
		}
		
		if(mouseX >= 13 * 34 + 40 && mouseX <= 13 * 34 + 40 + 34 && 
				mouseY >= 39 && mouseY <= 39 + 34 && Mouse.isButtonDown(0) &&
				!lastLeftState) {
			if(onMouse == null && result != null) {
				onMouse = new StackInfo(result.getId(), result.getCount());
				remove();
			} else if (result != null) {
				if(onMouse.getId() == result.getId()) {
					onMouse.add(result.getCount());
					remove();
				}
			}
		}
		
		lastLeftState = Mouse.isButtonDown(0);
		return onMouse;
	}
	
	private void remove() {
		for(int x = 0; x < 3; x++) {
			for(int y = 0; y < 3; y++) {
				if(crafting[x][y] != null) {
					crafting[x][y].setCount(crafting[x][y].getCount() - recipe.getStacks()[x][y].getCount());
					if(crafting[x][y].getCount() <= 0)
						crafting[x][y] = null;
				}
			}
		}
	}
	
}

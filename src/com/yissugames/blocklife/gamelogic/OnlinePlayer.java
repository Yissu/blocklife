package com.yissugames.blocklife.gamelogic;

import java.io.Serializable;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;

import com.yissugames.blocklife.BlockLife;
import com.yissugames.blocklife.RenderSystem;

public class OnlinePlayer implements Serializable{

	private String name;
	private float positionX;
	private float positionY;
	
	private Inventory inventory;
	
	//-------------- JUMP VARIABLES --------------//
	private enum JumpState { Up, Down, None }
	private boolean isJump;
	private JumpState jumpState = JumpState.None;
	private float beginJumpY;
	private final float jumpSpeed = 1.8f;
	private final float jumpHeight = 80.0f;
	
	public OnlinePlayer(String name, int positionX, int positionY)
	{
		this.name = name;
		this.positionX = positionX * 32;
		this.positionY = positionY * 32 - 64;
		
		this.inventory = new Inventory();
	}
	
	/**
	 * Call this at first
	 */
	public void begin(World world)
	{
		/*boolean left = Keyboard.isKeyDown(Keyboard.KEY_LEFT) || Keyboard.isKeyDown(Keyboard.KEY_A);
		boolean right = Keyboard.isKeyDown(Keyboard.KEY_RIGHT) || Keyboard.isKeyDown(Keyboard.KEY_D);
		boolean jump = Keyboard.isKeyDown(Keyboard.KEY_SPACE);
		
		
		boolean isBlocked = isNearChar(world, 1);
		if(left && this.positionX >= 0)
		{
			if(!isBlocked)
				this.positionX -= 0.8f;
		}
		if(right)
		{
			if(!isBlocked)
				this.positionX += 0.8f;
		}
		if(jump && !this.isJump)
		{
			this.isJump = true;
			this.jumpState = JumpState.Up;
			this.beginJumpY = positionY;
		}
		
		int positionX32 = (int) positionX / 32;
		int positionY32 = (int) positionY / 32;
		int chunkId = (int) (positionX32 / 32);
		
		if(this.isJump || !isBlocked)
		{
			if(this.jumpState == JumpState.Up)
			{
				this.positionY -= this.jumpSpeed;
				if(this.positionY <= this.beginJumpY - this.jumpHeight)
					this.jumpState = JumpState.Down;
			}
			if(this.jumpState == JumpState.Down || !isBlocked)
			{
				int height = world.get(chunkId).getHeightAt(positionX32 - (chunkId * 32));
				if(!isBlocked)
				{
					this.positionY += this.jumpSpeed * 0.5f;
				} else {
					this.isJump = false;
					this.jumpState = JumpState.None;
				}
			}
		}
		Display.setTitle("X: " + positionX32 + "Y: " + positionY32 + " Chunk: " + chunkId);*/
		
		//camera.setPosition(this.positionX, this.positionY);
		
		//RenderSystem.renderColoredQuad(positionX, positionY, 32, 64, 255, 0, 255);
	}
	
	/**
	 * mode 0: left bottom, right bottom
	 * mode 1: left side, right side
	 */
	private boolean isNearChar(World world, int mode)
	{
		int positionX32 = (int) positionX / 32;
		int positionX32andChar = ((int) positionX + 32) / 32;
		int chunkId = (int) (positionX32 / 32);
		int chunkId2 = (int) (positionX32andChar / 32);
		
		int height = world.get(chunkId).getHeightAt(positionX32 - (chunkId * 32));
		int height2 = world.get(chunkId2).getHeightAt(positionX32andChar - (chunkId2 * 32));
		
		boolean status = true;
		
		//Highest position is 0, if positionY >= height than is no blocked
		if(height * 32 >= this.positionY + this.jumpSpeed * 0.5f + 64 && mode == 0 || mode == 1)  // Left Bottom
			status = false;
		else if(mode == 0 || mode == 1)
			status = true;
		
		if(height2 * 32 >= this.positionY + this.jumpSpeed * 0.5f + 64 && mode == 0 || mode == 1) // Right Bottom
			status = false;
		else if(mode == 0 || mode == 1)
			status = true;
		
		/*if(height * 32 >= this.positionY + 32 + this.jumpSpeed * 0.5f + 64 && mode == 1) // Left Middle
			return true;
		
		if(height2 * 32 >= this.positionY + 32 + this.jumpSpeed * 0.5f + 64 && mode == 1) // Right Middle
			return true;
		
		if(height * 32 >= this.positionY + 64 + this.jumpSpeed * 0.5f + 64 && mode == 1) // Left Top
			return true;
		
		if(height2 * 32 >= this.positionY + 64 + this.jumpSpeed * 0.5f + 64 && mode == 1) // Right Top
			return true;*/
		
		return status;
	}
	
	/**
	 * Call this at last
	 */
	public void end()
	{
		
	}
	
	public Inventory getInventory()
	{
		return inventory;
	}
	
}

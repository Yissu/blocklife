/**
 * Copyright (c) 2012, http://www.yissugames.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yissugames.blocklife.gamelogic;

public class GrassBlock extends Block {

	private long lastRefreshTime = -1;
	
	public GrassBlock(String name, int id) {
		super(name, id);
	}
	
	public GrassBlock() {
		
	}
	
	@Override
	public int getDropId()
	{
		return 0;
	}

	@Override
	public void update(Chunk c) {
		if(lastRefreshTime == -1)
			lastRefreshTime = System.currentTimeMillis();
		
		if(positionX > 31 || positionY > 255 || positionX < 0 || positionY < 0)
			return;
		
		if(c.hasBlock(this.positionX, this.positionY - 1)) {
			if(lastRefreshTime + 7500 < System.currentTimeMillis())
			{
				c.destroy(this.positionX, this.positionY);
				c.addBlock(this.positionX, this.positionY, 0);
			}
		} else {
			lastRefreshTime = System.currentTimeMillis();
		}
	}

}

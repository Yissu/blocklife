/**
 * Copyright (c) 2012, http://www.yissugames.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yissugames.blocklife;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.lwjgl.Sys;
import org.lwjgl.opengl.Display;
import org.yaml.snakeyaml.Yaml;

public class Language {

	
	/**
	 * 0 - English
	 * 1 - German
	 */
	public static int LANGUAGE = 0;		//TODO: read this from settings
	private static HashMap<Object, Object> keys = new HashMap<Object, Object>();
	
	public static void loadLanguage()
	{
		try {
			InputStream input = new FileInputStream(new File("res/lang/" + LANGUAGE + ".yaml"));
			Yaml yaml = new Yaml();
			Iterable<Object> founds = yaml.loadAll(input);
			
			for(Object data: founds)
			{
				if(data instanceof HashMap) {
					HashMap<Object, Object> data2 = (HashMap<Object, Object>) data;
					for(Map.Entry e: data2.entrySet())
					{
						keys.put(e.getKey(), e.getValue());
					}
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			Display.destroy();
			Sys.alert("Error", "Can't load language file");
		}
	}
	
	public static boolean existsKey(String key)
	{
		return keys.containsKey(key);
	}
	
	public static String getValueAsString(String key)
	{
		if(keys.containsKey(key))
			return keys.get(key).toString();
		else 
			return key;
	}
	
}

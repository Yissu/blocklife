/**
 * Copyright (c) 2013, http://www.yissugames.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yissugames.blocklife;

import java.awt.Font;
import java.io.IOException;

import org.lwjgl.Sys;

import com.esotericsoftware.kryonet.Client;
import com.yissugames.blocklife.server.ShooterClient;
import com.yissugames.gui.Label;
import com.yissugames.gui.XMLScreen;

public class PublicServersScreen extends XMLScreen {

	private boolean showConnection;
	private Label s_label;
	private String content = "Connect to the server...";
	private String lastContent = "";
	
	private boolean nswitch;
	
	public PublicServersScreen()
	{
		super("PublicServers");
	}
	
	@Override
	public void onClick(int i) {
		switch(i) {
		case 1:
			showConnection = true;
			content = "Connect to the server...";
			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						ShooterClient.connect("127.0.0.1");
						showConnection = false;
						nswitch = true;
					} catch (IOException e) {
						content = "Can't connect to the server!";
						e.printStackTrace();
					}
				}
			}).start();
			break;
		case 51:
			ScreenSystem.set(new MainMenuScreen());
			break;
		}
	}

	@Override
	public void specialRender() {
		if(nswitch) {
			ScreenSystem.set(new GameScreen());
		}
		
		if(showConnection) {
			if(lastContent != content) 
				s_label = new Label("Arial", 24, Font.BOLD, "yellow", content, -2, -2);
			s_label.render();
		}
	}

	@Override
	public boolean specialInit() {
		return true;
	}

}

/**
 * Copyright (c) 2012, http://www.yissugames.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yissugames.blocklife;

import java.util.ArrayList;
import java.util.Random;

import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.opengl.Texture;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.yissugames.blocklife.server.AddBlock;
import com.yissugames.blocklife.server.BeginWorld;
import com.yissugames.blocklife.server.ChatPackage;
import com.yissugames.blocklife.server.ConnectResponse;
import com.yissugames.blocklife.server.ConnectRequest;
import com.yissugames.blocklife.server.DestroyBlock;
import com.yissugames.blocklife.server.EndWorld;
import com.yissugames.blocklife.server.ShooterClient;
import com.yissugames.blocklife.gamelogic.Block;
import com.yissugames.blocklife.gamelogic.BlockCamera;
import com.yissugames.blocklife.gamelogic.Chunk;
import com.yissugames.blocklife.gamelogic.SerializableWorld;
import com.yissugames.blocklife.gamelogic.World;
import com.yissugames.gui.Button;
import com.yissugames.gui.Event;
import com.yissugames.gui.GUI;
import com.yissugames.gui.Label;

public class GameScreen implements Screen {

	private World world;
	
	private boolean showESCmenu;
	private boolean chatInput;
	
	private Button backToGame;
	private Button backToMenu;
	private Button craftingRecipes;
	
	private BlockCamera camera;
	private ArrayList<Chunk> chunks = new ArrayList<Chunk>();
	private ArrayList<ChatPackage> messages = new ArrayList<ChatPackage>();
	
	private Label loadWorld;
	private int allChunks = 1;
	private boolean finish;
	
	private Font chatFont;
	private String allowedChatChars = "abcdefghijklmnopqrstuvwxyz���ABCDEFGHIJKLMNOPQRSTUVWXYZ���1234567890 !?.,:-/()[]*^&{}%$=<>#~+\"'@|";
	private String chatmessage;
	
	public GameScreen() {
		
		chatFont = ContentLoader.getFont("Arial", 0, 12);
		
		Event e1 = new Event()
		{
			@Override
			public void onClick(GUI g) {
				showESCmenu = false;
			}
		};
		this.backToGame = new Button(Display.getWidth() / 2 - 250 / 2, Display.getHeight() / 2 - 50 / 2 - 23, 250, 36, "Back to Game", e1);
		
		Event e2 = new Event()
		{
			@Override
			public void onClick(GUI g) {
				ScreenSystem.set(new MainMenuScreen());
			}
		};
		this.backToMenu = new Button(Display.getWidth() / 2 - 250 / 2, Display.getHeight() / 2 - 50 / 2 + 23, 250, 36, "Back to Main Menu", e2);
		
		Event e3 = new Event()
		{
			@Override
			public void onClick(GUI g) {
				ScreenSystem.setAndSave(new CraftingRecipesScreen());
			}
		};
		this.craftingRecipes = new Button(Display.getWidth() - 260, Display.getHeight() - 46, 250, 36, "Crafting Recipes", e3);
	}
	
	private int calculateShowenMessages() {
		int i = 0;
		for(ChatPackage message: messages) {
			if(message.initTime + 10000 > System.currentTimeMillis()) {
				i++;
			}
		}
		return i > 15 ? 16 : i;
	}
	
	private void sendInput(char c) {
		if(allowedChatChars.contains("" + c))
			chatmessage += c;
		
		if((int) c == 8 && chatmessage.length() > 0)
			chatmessage = chatmessage.substring(0, chatmessage.length() - 1);
	}
	
	@Override
	public void render() {
		
		if(finish && world == null) {
			world = new World(new SerializableWorld(chunks));
		}
		
		if(world == null) {
			loadWorld = new Label("Arial", 16, 1, "white", "Loading World... (" + (chunks.size() / allChunks * 100) + "%)", -1, -1);
			if(loadWorld != null)
				loadWorld.render();
			return;
		}
		
		while(Keyboard.next()) {
			if(Keyboard.isKeyDown(Keyboard.KEY_ESCAPE))
				showESCmenu = !showESCmenu;
			if(Keyboard.isKeyDown(Keyboard.KEY_RETURN)) {
				if(chatInput && chatmessage.length() > 0)
					ShooterClient.sendMessage(chatmessage);
				chatInput = !chatInput;
				chatmessage = "";
			}
			//for testing
			if(Keyboard.isKeyDown(Keyboard.KEY_F6)) 
				ShooterClient.sendMessage("Test Message");
			if(chatInput) {
				sendInput(Keyboard.getEventCharacter());
			}
		}
		
		world.render(showESCmenu, chatInput);
		
		Texture tex = ContentLoader.getTexture("70black");
		int posY = Display.getHeight() - chatFont.getLineHeight() * 2 - (chatFont.getLineHeight() * this.calculateShowenMessages());
		if(this.calculateShowenMessages() != 0 || chatInput)
			RenderSystem.renderTexture(tex, 0, posY, 500, Display.getHeight() * 2);
		if(chatInput) 
			chatFont.drawString(5, BlockLife.DisplayHeight - 5 - chatFont.getLineHeight(), chatmessage + "|", Color.white);
		
		int messagesRendered = 0;
		for(int i = messages.size() - 1; i >= 0; i--)
		{
			ChatPackage message = messages.get(i);
			if(message.initTime + 10000 > System.currentTimeMillis()) {
				chatFont.drawString(5, BlockLife.DisplayHeight - 5 - chatFont.getLineHeight() * 2 - (chatFont.getLineHeight() * messagesRendered), "[" + message.date + "][" + message.sender + "] " + message.message, Color.white);
				
				messagesRendered++;
			}
			if(messagesRendered > 15)
				break;
		}
		if(messagesRendered == 0) {
			messages.clear();	// Clear the messages when nothing rendered
		}
		
		
		if(showESCmenu)
		{
			RenderSystem.renderColoredQuad(0, 0, BlockLife.DisplayWidth, BlockLife.DisplayHeight, 0f, 0f, 0f, 0.6f);
			backToGame.render();
			backToMenu.render();
			craftingRecipes.render();
		}
	}

	@Override
	public boolean init() {
		Block.LoadBlocks();
		
		ShooterClient.addListener(new Listener() {
			public void received (Connection connection, Object object) {
				if(object instanceof ConnectResponse) {
					ShooterClient.setIdentify(((ConnectResponse) object).sessionId);
					System.out.println(ShooterClient.getSessionId());
				}
				
				if(object instanceof BeginWorld) {
					allChunks = ((BeginWorld) object).chunks;
				}
				
				if(object instanceof Chunk) {
					chunks.add((Chunk) object);
				}
				
				if(object instanceof EndWorld) {
					finish = true;
				}
				
				if(object instanceof DestroyBlock) {
					DestroyBlock request = (DestroyBlock) object;
					world.get(request.chunk).destroy(request.x, request.y);
				}
				
				if(object instanceof AddBlock) {
					AddBlock request = (AddBlock) object;
					world.get(request.chunk).addBlock(request.x, request.y, request.block);
				}
				
				if(object instanceof ChatPackage) {
					ChatPackage pack = (ChatPackage) object;
					pack.init();
					messages.add(pack);
					System.out.println("[" + pack.date + "][" + pack.sender + "] " + pack.message);
				}
			}
		});
		ShooterClient.setUsername("Non Public " + new Random(System.currentTimeMillis()).nextInt(1000));
		ShooterClient.send(new ConnectRequest(ShooterClient.getUsername()));
		
		BlockLife.BackgroundR = 0;
		BlockLife.BackgroundG = 0;
		BlockLife.BackgroundB = 0;
		
		return true;
	}

	@Override
	public boolean quit() {
		BlockLife.BackgroundR = 1;
		BlockLife.BackgroundG = 1;
		BlockLife.BackgroundB = 1;
		
		return true;
	}

}

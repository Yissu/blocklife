/**
 * Copyright (c) 2012, http://www.yissugames.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yissugames.blocklife;

import static org.lwjgl.opengl.GL11.*;

import org.newdawn.slick.opengl.Texture;

public class RenderSystem {

	public static void renderTexture(Texture texture, int x, int y)
	{
		texture.bind();
		glColor3f(1, 1, 1);
		glBegin(GL_QUADS);
		glTexCoord2f(0, 0);
		glVertex2i(x, y);
		glTexCoord2f(texture.getWidth(), 0);
		glVertex2i(x + texture.getImageWidth(), y);
		glTexCoord2f(texture.getWidth(),  texture.getHeight());
		glVertex2i(x + texture.getImageWidth(), y + texture.getImageHeight());
		glTexCoord2f(0, texture.getHeight());
		glVertex2i(x, y + texture.getImageHeight());
		glEnd();
	}
	
	/**
	 * Warning! Has an render bug!
	 */
	public static void renderTexture(Texture texture, int x, int y, int width, int height)
	{
		texture.bind();
		glColor3f(1, 1, 1);
		glBegin(GL_QUADS);
		glTexCoord2f(0, 0);
		glVertex2i(x, y);
		glTexCoord2f(0, 0);
		glVertex2i(x + width, y);
		glTexCoord2f(0,  1);
		glVertex2i(x + width, y + height);
		glTexCoord2f(0, 1);
		glVertex2i(x, y + height);
		glEnd();
	}
	
	public static void renderRTexture(Texture texture, int posX, int posY, int rectX, int rectY, int rectWidth, int rectHeight)
	{
		texture.bind();
		
		float xNull = (texture.getWidth() / texture.getImageWidth()) * rectX;
		float xOne = (texture.getWidth() / texture.getImageWidth()) * (rectX + rectWidth );
		float yNull = (texture.getHeight() / texture.getImageHeight()) * rectY;
		float yOne = (texture.getHeight() / texture.getImageHeight()) * (rectY + rectHeight);
		
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		
		glColor3f(1, 1, 1);
		glBegin(GL_QUADS);
		glTexCoord2f(xNull, yNull);
		glVertex2i(posX, posY);
		glTexCoord2f(xOne, yNull);
		glVertex2i(posX + rectWidth, posY);
		glTexCoord2f(xOne,  yOne);
		glVertex2i(posX + rectWidth, posY + rectHeight);
		glTexCoord2f(xNull, yOne);
		glVertex2i(posX, posY + rectHeight);
		glEnd();
	}
	
	public static void renderTileTexture(Texture texture, int x, int y, int width, int height, int tId)
	{
		texture.bind();
		
		float xNull = texture.getWidth() / 20 * tId;
		float xOne = texture.getWidth() / 20 * (tId + 1);
		float yNull = 0;
		float yOne = texture.getHeight() / 20;
		
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		
		glColor3f(1, 1, 1);
		glBegin(GL_QUADS);
		glTexCoord2f(xNull, yNull);
		glVertex2i(x, y);
		glTexCoord2f(xOne, yNull);
		glVertex2i(x + width, y);
		glTexCoord2f(xOne,  yOne);
		glVertex2i(x + width, y + height);
		glTexCoord2f(xNull, yOne);
		glVertex2i(x, y + height);
		glEnd();
	}
	
	public static void renderColoredQuad(float x, float y, int width, int height, int r, int g, int b)
	{
		glDisable(GL_TEXTURE_2D);
		glColor3ub((byte) r, (byte) g, (byte) b);
		//glColor3f(multi * (float) r, multi * (float) g, multi * (float) b);
		glBegin(GL_QUADS);
		glVertex2f(x, y);
		glVertex2f(x + width, y);
		glVertex2f(x + width, y + height);
		glVertex2f(x, y + height);
		glEnd();
		glEnable(GL_TEXTURE_2D);
	}
	
	public static void renderColoredQuad(int x, int y, int width, int height, float r, float g, float b, float a)
	{
		glDisable(GL_TEXTURE_2D);
		glColor4f(r, g, b, a);
		glBegin(GL_QUADS);
		glVertex2i(x, y);
		glVertex2i(x + width, y);
		glVertex2i(x + width, y + height);
		glVertex2i(x, y + height);
		glEnd();
		glEnable(GL_TEXTURE_2D);
	}
	
	public static void renderLine(int startx, int starty, int endx, int endy, int r, int g, int b)
	{
		glDisable(GL_TEXTURE_2D);
		glColor3ub((byte) r, (byte) g, (byte) b);
		glBegin(GL_LINES);
		glVertex2i(startx, starty);
		glVertex2i(endx, endy);
		glEnd();
		glEnable(GL_TEXTURE_2D);
	}
}

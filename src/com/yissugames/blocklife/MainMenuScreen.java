/**
 * Copyright (c) 2012, http://www.yissugames.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yissugames.blocklife;

import java.util.ArrayList;
import java.util.Random;

import org.lwjgl.opengl.Display;

import com.yissugames.gui.AnimatedImage;
import com.yissugames.gui.XMLScreen;

/**
 * It is the main menu
 * 
 * @author Jonas
 * @version 1.0
 */
public class MainMenuScreen extends XMLScreen {
	
	private long lastSpawn;
	
	private Random rand = new Random();
	
	private ArrayList<AnimatedImage> ammunitions = new ArrayList<AnimatedImage>();
	
	public MainMenuScreen() {
		super("MainMenu");
	}

	@Override
	public void onClick(int i) {
		switch(i)
		{
		case 0:
			ScreenSystem.set(new PublicServersScreen());
			break;
		case 1:
			Display.destroy();
			System.exit(0);
		}
	}

	@Override
	public void specialRender() {
		if(lastSpawn + 2000 <= System.currentTimeMillis()) {
			lastSpawn = System.currentTimeMillis();
			if(rand.nextBoolean()) {
				ammunitions.add(new AnimatedImage(-30, 830, rand.nextInt(300), rand.nextInt(10) + 5, "ammunition1", true));
			} else {
				ammunitions.add(new AnimatedImage(830, -30, rand.nextInt(300), rand.nextInt(10) + 5, "ammunition1", true));
			}
			System.out.println("Next spawn");
		}
		
		for(AnimatedImage ammunition: ammunitions) {
			ammunition.render();
		}
	}

	@Override
	public boolean specialInit() {
		return true;
	}

}

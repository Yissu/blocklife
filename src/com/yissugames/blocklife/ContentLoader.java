/**
 * Copyright (c) 2012, http://www.yissugames.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yissugames.blocklife;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.openal.Audio;
import org.newdawn.slick.openal.AudioLoader;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

public class ContentLoader {

	private static HashMap<String, Texture> textures = new HashMap<String, Texture>();
	private static HashMap<String, TrueTypeFont> fonts = new HashMap<String, TrueTypeFont>();
	private static HashMap<String, Audio> audios = new HashMap<String, Audio>();
	
	public static void loadAll() {
		getTexture("background");
		getTexture("BlockLife");
		getTexture("chckBox.hover");
		getTexture("chckBox.mark");
		getTexture("cloud0");
		getTexture("cloud1");
		getTexture("coBox.bg");
		getTexture("coBox.item.old");
		getTexture("coBox.item");
		getTexture("crafting");
		getTexture("himmel");
		getTexture("inventory.grid");
		getTexture("quickbar.grid");
		getTexture("quickbar.grid.select");
		getTexture("quickbar.text");
		getTexture("slider.mark");
		getTexture("slider");
		getTexture("texture");
		getTexture("txtBox");
	}
	
	public static String[] getLoadedObjects()
	{
		String[] objects = new String[textures.size() + fonts.size()];
		ArrayList<Set<String>> tmp = new ArrayList<Set<String>>();
		tmp.add(textures.keySet());
		tmp.add(fonts.keySet());
		tmp.add(audios.keySet());
		
		int x = 0;
		for(Set<String> tmp2: tmp)
		{
			for(String s: tmp2)
			{
				objects[x] = s;
				x++;
			}
		}
		
		return objects;
	}
	
	public static Texture getTexture(String name)
	{
		if(textures.containsKey(name))
		{
			return textures.get(name);
		}
		
		try {
			Texture tmpTexture = TextureLoader.getTexture("PNG", new FileInputStream(new File("res/textures/" + name + ".png")));
			textures.put(name, tmpTexture);
			return textures.get(name);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Audio getOGGAudio(String audioFile, boolean voice)
	{
		String assetName = audioFile + ":" + voice;
		if(audios.containsKey(assetName))
			return audios.get(assetName);
		
		String subfolder = voice ? "voice/" : "";
		try {
			Audio audiostream = AudioLoader.getAudio("OGG", ResourceLoader.getResourceAsStream("res/sounds/" + subfolder + audioFile + ".ogg"));
			audios.put(assetName, audiostream);
			return audios.get(assetName);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static TrueTypeFont getFont(String fontName, int style, int size)
	{
		String assetName = fontName + ":" + style + ":" + size;
		if(fonts.containsKey(assetName))
			return fonts.get(assetName);
		
		Font awtFont = new Font(fontName, style, size);
		TrueTypeFont tmpFont = new TrueTypeFont(awtFont, true);
		fonts.put(assetName, tmpFont);
		
		return fonts.get(assetName);
	}
	
	public static TrueTypeFont getExternFont(String fontFile, int size)
	{
		int style = 0;
		
		String assetName = fontFile + ":" + style + ":" + size;
		if(fonts.containsKey(assetName))
			return fonts.get(assetName);
		
		InputStream inputStream = ResourceLoader.getResourceAsStream("res/ttf/" + fontFile + ".ttf");
		
		try {
			Font awtFont = Font.createFont(Font.TRUETYPE_FONT, inputStream);
			awtFont = awtFont.deriveFont(size);
			TrueTypeFont tmpFont = new TrueTypeFont(awtFont, true);
			fonts.put(assetName, tmpFont);
			
			return fonts.get(assetName);
		} catch (FontFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
}

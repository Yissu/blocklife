/**
 * Copyright (c) 2012, http://www.yissugames.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yissugames.settings;

import flexjson.*;
import java.io.*;
import java.util.ArrayList;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

public class SettingsSystem {

	public static void writeToFile(SettingsFile f, String path) throws Exception{
		PrintWriter w = new PrintWriter(path);
		w.println(new JSONSerializer().deepSerialize(f));
		w.flush();
		w.close();
	}
	
	public static SettingsFile readFromFile(String path) throws Exception {
		String content = null;
		String t;
		FileInputStream fstream = new FileInputStream(path);
		DataInputStream in = new DataInputStream(fstream);
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		while (!((t = br.readLine()) == null)){
			content += t;
		}
		in.close();
		SettingsFile s = new JSONDeserializer<SettingsFile>().deserialize(content);
		return s;		
	}
	
	public static void main(String[] args) throws Exception {
		SettingsFile s = new SettingsFile();
		s.setFullscreen(false);
		DisplayMode[] modes = Display.getAvailableDisplayModes();
		ArrayList<DisplayMode> nmode = new ArrayList<DisplayMode>();
		//Remove sames
		for (DisplayMode m : modes){
			
		}
		
		for (DisplayMode m : modes){
			System.out.println(m.getWidth() + "x" + m.getHeight() + "x" + m.getBitsPerPixel());
		}
	}
	
}

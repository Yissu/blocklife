/**
 * Copyright (c) 2012, http://www.yissugames.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yissugames.gui;

import java.awt.Rectangle;

import org.jdom2.Element;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.opengl.Texture;

import com.yissugames.blocklife.BlockLife;
import com.yissugames.blocklife.RenderSystem;
import com.yissugames.blocklife.ContentLoader;

public class CheckBox implements GUI{

	private Texture texture = ContentLoader.getTexture("chckBox");
	private Texture mark = ContentLoader.getTexture("chckBox.mark");
	private Texture hover = ContentLoader.getTexture("chckBox.hover");
	private int posX, posY;
	private boolean selected = false;
	private boolean now = false;
	private boolean last = false;	
	private String text;
	private TrueTypeFont font;
	private Event c;
	
	public CheckBox(int posX, int posY, Event c)
	{
		this.posX = posX;
		this.posY = posY;
		this.c = c;
	}
	
	public CheckBox(int posX, int posY, Event c, String text)
	{
		this.posX = posX;
		this.posY = posY;
		this.text = text;
		this.font = ContentLoader.getFont(BlockLife.font, 0, 24);
		this.c = c;
	}
	
	@Override
	public void render() {
		Rectangle bounds = new Rectangle(posX, posY, texture.getImageWidth(), texture.getImageHeight());
		now = Mouse.isButtonDown(0);
		//Render the background
		RenderSystem.renderTexture(texture, posX, posY);
		if (!(font == null)){
			int ty = posY + ((texture.getImageHeight() / 2) - (font.getHeight(text) / 2));
			font.drawString(posX + texture.getImageWidth() + 2, ty, text);
		}
		if (this.selected == true)
			RenderSystem.renderTexture(mark, posX, posY);
		if(bounds.contains(Mouse.getX(), BlockLife.DisplayHeight - Mouse.getY() - 1)){
			//Mouse is over it
			if (now && last == false){
				//Left button is pressed
				selected = invert(selected);
				this.c.onStateChanged(this, selected);
			}
			if (this.selected == false)
				RenderSystem.renderTexture(hover, posX, posY);
		}
		
		last = now;
	}
		
	public boolean invert(boolean b){
		if (b == true)
			return false;
		else
			return true;
	}
	
	public void setTexture(Texture texture)
	{
		this.texture = texture;
	}
	
	public static CheckBox createByXML(Element e)
	{
		int posX = -1;
		if(!e.getAttributeValue("positionX").equals("center"))
			posX = Integer.parseInt(e.getAttributeValue("positionX"));
		int posY = -1;
		if(!e.getAttributeValue("positionY").equals("center"))
			posY = Integer.parseInt(e.getAttributeValue("positionY"));
		Event c = null;
		
		return new CheckBox(posX, posY, c, e.getText());
	}

}

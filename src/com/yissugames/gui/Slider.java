/**
 * Copyright (c) 2012, http://www.yissugames.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yissugames.gui;

import java.awt.Rectangle;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.opengl.Texture;

import com.yissugames.blocklife.BlockLife;
import com.yissugames.blocklife.RenderSystem;
import com.yissugames.blocklife.ContentLoader;

public class Slider implements GUI{

	private Texture texture = ContentLoader.getTexture("slider");
	private Texture mark = ContentLoader.getTexture("slider.mark");
	private int posX, posY;
	private int width;
	private int markX;

	/**
	 * @deprecated Unusable
	 * @param posX
	 * @param posY
	 * @param width
	 */
	public Slider(int posX, int posY, int width)
	{
		this.posX = posX;
		this.posY = posY;
		this.width = width;
		this.markX = posX;
	}
	
	@Override
	public void render() {
		Rectangle bounds = new Rectangle(markX, posY, mark.getImageWidth() + 10, mark.getImageHeight() + 10);
		if(bounds.contains(Mouse.getX(), BlockLife.DisplayHeight - Mouse.getY() - 1)){
			//Mouse is over it
			//Left button is pressed
			if (Mouse.isButtonDown(0)){
				this.markX = Mouse.getX();
			}
		}
		//Render the background
		RenderSystem.renderTexture(texture, posX, posY);
		RenderSystem.renderTexture(mark, (markX - (mark.getImageWidth() / 2)), posY);
	}
		
	public boolean invert(boolean b){
		if (b == true)
			return false;
		else
			return true;
	}
	
	public void setTexture(Texture texture)
	{
		this.texture = texture;
	}

	public int getMarkX(){
		return markX;
	}
	
}

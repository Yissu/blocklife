/**
 * Copyright (c) 2012, http://www.yissugames.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yissugames.gui;

import java.awt.Font;

import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;

import com.yissugames.blocklife.BlockLife;
import com.yissugames.blocklife.ContentLoader;
import com.yissugames.blocklife.Language;

public class Label implements GUI
{
	private TrueTypeFont font;
	private String string;
	private Color color;
	private int x;
	private int y;
	
	public Label(String fontName, int fontSize, String color, String content, int posX, int posY)
	{
		font = ContentLoader.getFont(fontName, 0, fontSize);
		string = content;
		if(color.equals("black"))
			this.color = Color.black;
		else if(color.equals("white"))
			this.color = Color.white;
		else if(color.equals("yellow"))
			this.color = Color.yellow;
		
		x = posX;
		y = posY;
	}
	
	public Label(String fontName, int fontSize, int fontStyle, String color, String content, int posX, int posY)
	{
		font = ContentLoader.getFont(fontName, fontStyle, fontSize);
		string = content;
		if(color.equals("black"))
			this.color = Color.black;
		else if(color.equals("white"))
			this.color = Color.white;
		else if(color.equals("yellow"))
			this.color = Color.yellow;
		
		x = posX;
		y = posY;
	}

	@Override
	public void render() {
		int posX = x;
		int posY = y;
		if(posX == -1)
			posX = BlockLife.DisplayWidth / 2 - font.getWidth(string) / 2;
		if(posX == -2)
			posX = BlockLife.DisplayWidth - font.getWidth(string);
		if(posY == -1)
			posY = BlockLife.DisplayHeight / 2 - font.getHeight() / 2;
		if(posY == -2)
			posY = BlockLife.DisplayHeight - font.getHeight();
		
		font.drawString(posX, posY, Language.getValueAsString(string), color);
		
	}
}
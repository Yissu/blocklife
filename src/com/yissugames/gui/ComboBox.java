/**
 * Copyright (c) 2012, http://www.yissugames.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yissugames.gui;

import java.awt.Rectangle;
import java.util.ArrayList;

import org.jdom2.Element;
import org.lwjgl.input.Mouse;
import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.opengl.Texture;

import com.yissugames.blocklife.BlockLife;
import com.yissugames.blocklife.ContentLoader;
import com.yissugames.blocklife.Language;
import com.yissugames.blocklife.RenderSystem;

public class ComboBox implements GUI {

	public static final int TEXT_DISTANCE = 3;
	private int posX;
	private int posY;
	private ArrayList<ComboBoxItem> items = new ArrayList<ComboBoxItem>();
	private int itemCount;
	private int currentItem;
	private TrueTypeFont font = ContentLoader.getFont(BlockLife.font, 0, 24);
	private Texture background = ContentLoader.getTexture("coBox.bg");
	private Texture dropdown = ContentLoader.getTexture("coBox.item");
	private Texture hover = ContentLoader.getTexture("coBox.item.hover");
	private Texture old = ContentLoader.getTexture("coBox.item.old");
	private boolean opened = false;;
	private Event c;
	
	// Mouse events
	private boolean last = false;
	private boolean now = false;

	/**
	 * State changed
	 * @param posX X position
	 * @param posY Y Position
	 * @param items The items
	 * @param c The event
	 */
	public ComboBox(int posX, int posY, ArrayList<ComboBoxItem> items, Event c) {
		this.posX = posX;
		this.posY = posY;
		this.items = items;
		this.itemCount = items.size();
		this.c = c;
	}

	public ComboBoxItem getItem() {
		return items.get(currentItem);
	}

	public int getItemIndex() {
		return currentItem;
	}

	public void setItemIndex(int i) {
		this.currentItem = i;
	}

	public boolean invert(boolean b) {
		if (b == true)
			return false;
		else
			return true;
	}

	public void setItems(ArrayList<ComboBoxItem> items) {
		this.items = items;
		this.itemCount = items.size();
	}

	@Override
	public void render() {
		now = Mouse.isButtonDown(0);

		// Body
		if (opened == false) {
			Rectangle bounds = new Rectangle(posX, posY,
					background.getImageWidth(), background.getImageHeight());
			// ComboBox is closed
			// Render the background
			RenderSystem.renderTexture(background, posX, posY);
			font.drawString(posX + TEXT_DISTANCE, posY + 5, Language.getValueAsString(
					items.get(currentItem).getText()), Color.black);

			if (bounds.contains(Mouse.getX(), BlockLife.DisplayHeight- Mouse.getY() - 1)){
				//Mouse is hovered
				if (now && last == false) {
					// Mouse is down
					if(c != null) c.onStateChanged(this, opened);
					opened = invert(opened);
				}
			}
			
		} else {
			Rectangle bounds = new Rectangle(posX, posY,
					dropdown.getImageWidth(), dropdown.getImageHeight());
			int relY;
			
			int t = posY;
			for (ComboBoxItem i : items) {
				if (i == getItem()){
					//It's the previous selected item
					RenderSystem.renderTexture(old, posX, t);
				}else{
					//It's another item
					RenderSystem.renderTexture(dropdown, posX, t);
				}
				font.drawString(posX + TEXT_DISTANCE, t + 5, Language.getValueAsString(i.getText()), Color.black);
				t += 34;
			}
			// Is in the width of the control?
			if ((Mouse.getX() <= posX + dropdown.getImageWidth())
					&& (Mouse.getX() >= posX)) {
				// Is in the Height?
				int y = BlockLife.DisplayHeight - Mouse.getY() - 1;
				if ((y <= posY + (dropdown.getImageHeight() * itemCount))
						&& (y >= posY)) {
					//Positions etc.
					relY = BlockLife.DisplayHeight - Mouse.getY() - 1;
					relY = relY - posY;
					relY = relY / 34;
					RenderSystem.renderTexture(hover, posX , ((posY) + (relY * 34)));
					if (relY <= itemCount - 1){
						font.drawString(posX + TEXT_DISTANCE, ((posY) + (relY * 34)) + 5, Language.getValueAsString(items.get(relY).getText()), Color.black);
						//Mouse is down
						if (now && last == false) {
							if(c != null) c.onStateChanged(this, opened);
							currentItem = relY;
							opened = invert(opened);
						}
					}
				}
			}
		}
		last = now;
	}
	
	public static ArrayList<ComboBoxItem> fillWithDisplayModes()
	{
		return null;
	}
	
	public static ComboBox createByXML(Element e)
	{
		int posX = -1;
		if(!e.getAttributeValue("positionX").equals("center"))
			posX = Integer.parseInt(e.getAttributeValue("positionX"));
		int posY = -1;
		if(!e.getAttributeValue("positionY").equals("center"))
			posY = Integer.parseInt(e.getAttributeValue("positionY"));
		
		ArrayList<ComboBoxItem> items = new ArrayList<ComboBoxItem>();
		if(e.getChildren("datasource").size() > 0)
		{
			for(Element e2: e.getChildren("datasource"))
			{
				String dataSource = e2.getText();
				if(dataSource.equals("Display.getAvalibleModes()"))
					items.addAll(fillWithDisplayModes());
			}
				
		}
		for(Element e2: e.getChildren("item"))
			items.add(new ComboBoxItem(e2.getText(), e2.getAttributeValue("id")));
		
		return new ComboBox(posX, posY, items, null);
	}
}
/**
 * Copyright (c) 2012, http://www.yissugames.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yissugames.gui;

import org.newdawn.slick.opengl.Texture;

import com.yissugames.blocklife.BlockLife;
import com.yissugames.blocklife.ContentLoader;
import com.yissugames.blocklife.RenderSystem;

public class Image implements GUI
{
	private Texture texture;
	private int x;
	private int y;
	
	public Image(String asset, int positionX, int positionY)
	{
		texture = ContentLoader.getTexture(asset);
		x = positionX;
		y = positionY;
	}
	
	@Override
	public void render()
	{
		int posX = x;
		int posY = y;
		if(posX == -1)
			posX = BlockLife.DisplayWidth / 2 - texture.getImageWidth() / 2;
		if(posY == -1)
			posY = BlockLife.DisplayHeight / 2 - texture.getImageHeight() / 2;
		
		RenderSystem.renderTexture(texture, posX, posY);
	}
}
/**
 * Copyright (c) 2012, http://www.yissugames.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yissugames.gui;

import java.awt.Rectangle;

import org.jdom2.Element;
import org.lwjgl.input.Mouse;
import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.opengl.Texture;

import com.yissugames.blocklife.BlockLife;
import com.yissugames.blocklife.ContentLoader;
import com.yissugames.blocklife.Language;
import com.yissugames.blocklife.RenderSystem;

public class Button implements GUI {

	private int posX;
	private int posY;
	private int height;
	private int width;
	private boolean now, now2;
	private boolean last, last2;
	private Event c;
	private String text;
	private TrueTypeFont font = ContentLoader.getFont(BlockLife.font, 0, 24);
	
	public Button(int posX, int posY,int width, int height, String text, Event c){
		this.posX = posX;
		this.posY = posY;
		this.height = height;
		this.width = width;
		this.text = text;
		this.c = c;
	}
	
	public void setEvent(Event c)
	{
		this.c = c;
	}
	
	@Override
	public void render() {
		now = Mouse.isButtonDown(0);
		now2 = Mouse.isButtonDown(1);
		Rectangle bounds = new Rectangle(posX, posY, width, height);
		if (bounds.contains(Mouse.getX(), BlockLife.DisplayHeight - Mouse.getY())){
			//Mouse is over
			RenderSystem.renderColoredQuad(posX, posY, width, height, 170, 170, 170);
			if (now && !last && c != null){
				//Mouse is clicked
				this.c.onClick(this);
			}else if (now2 && !last2 && c != null){
				//Mouse is right clicked
				this.c.onRightClick(this);
			}
		}else{
			RenderSystem.renderColoredQuad(posX, posY, width, height, 255, 255, 255);
		}
		int tx = posX + ((width / 2) - (font.getWidth(Language.getValueAsString(text)) / 2));
		int ty = posY + ((height / 2) - (font.getHeight(Language.getValueAsString(text)) / 2));
		font.drawString(tx, ty, Language.getValueAsString(text), Color.black);
		last = now;
		last2 = now2;
	}
	
	public static Button createByXML(Element e)
	{
		int posX = -1;
		if(!e.getAttributeValue("positionX").equals("center"))
			posX = Integer.parseInt(e.getAttributeValue("positionX"));
		int posY = -1;
		if(!e.getAttributeValue("positionY").equals("center"))
			posY = Integer.parseInt(e.getAttributeValue("positionY"));
		int sizeX = Integer.parseInt(e.getAttributeValue("sizeX"));
		int sizeY = Integer.parseInt(e.getAttributeValue("sizeY"));
		
		return new Button(posX, posY, sizeX, sizeY, e.getText(), null);
	}

}
